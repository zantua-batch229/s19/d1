//console.log("asdf");

// [SECTION] - If, else if, else

let numG = -1;
let numH = 1;

if (numG < 0) {
	console.log("True");
}

// else if statement

/*


*/
if (numG > 0) {
	console.log("Hello");
}else if (numH > 0){
	if (numG < 0) {
		console.log("GWorld");	
	} else {
		console.log("HWorld");	
	}
}else {
	console.log("FalseWorld");
}

let message;


function determineTyphoonIntensity(windspeed){

	if (windspeed < 30){
		return "Not a typhoon yet";
	}else if (windspeed <= 61){
		return "Tropical depression detected";
	}else if (windspeed >= 62 && windspeed <= 88){
		return "Tropical storm detected";
	}else if (windspeed >= 89 && windspeed <= 117){
		return "Severe Tropical storm detected";
	}else {
		return "Typhoon detected";
	}

}

// Return the string to the variable "message" that invoke it
message = determineTyphoonIntensity(116);
console.log(message);

if (message == "Severe Tropical storm detected"){
	console.warn(message);
}


// [SECTION] Truthy and Falsy
/* 
- In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
- Values are considered true unless defined otherwise
- Falsy values/exceptions for truthy:
    1. false
    2. 0
    3. -0
    4. ""
    5. null
    6. undefined
    7. NaN
*/
// truthy examples
 if (true) {
	console.log("truthy");
}

if (1) {
	console.log("truthy");
}
if ([]) {
	console.log("truthy");
}
// falsy examples

if (false) {
	console.log("falsy");
}
if (0) {
	console.log("falsy");
}
if (undefined) {
	console.log("falsy");
}
if (NaN) {
	console.log("falsy");
}

// [SECTION] - Conditional (Ternary) operator
/* 
- The Conditional (Ternary) Operator takes in three operands:
    1. condition
    2. expression to execute if the condition is truthy
    3. expression to execute if the condition is falsy
- Can be used as an alternative to an "if else" statement
- Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
- Commonly used for single statement execution where the result consists of only one line of code
- For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
- Syntax
    (expression) ? ifTrue : ifFalse;
*/

// single statement execution

let ternaryResult1 = (1 < 2) ? true : false;
console.log("The resulet of ternary1 is: " + ternaryResult1);
let ternaryResult2 = (1 < 2) ? console.log("less than") : console.log("less than");

// multiple statement execution
// both functions perform two separate tasks which changes the value of the "name" variable and returns result storing it in the "legalAge" var

let name;


function isOfLegalAge(){
	name = "johm";
	return "You are of legal age limit";
}
function isUnderAge(){
	name = "jane";
	return "You are under age limit";
}

//let age = parseInt(prompt("What is your age?"));
//console.log(typeof age);

//let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
//console.log(name + " " + legalAge);

/* 
	- Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
	- The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
	- The "expression" is the information used to match the "value" provided in the switch cases
	- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
	- Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
	- The "break" statement is used to terminate the current loop once a match has been found
	- Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
	- Syntax
	    switch (expression) {
	        case value:
	            statement;
	            break;
	        default:
	            statement;
	            break;
	    }
*/
let day = prompt("What day is it?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;	
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
		break;
}
// [SECTION] Try-Catch-Finally statement

function showIntensityAlert(windspeed) {
	try{
		// attempt to execute the codes
		alert(determineTyphoonIntensity(windspeed));
	}catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}finally {
		alert("Intensity updates will show new alert");
	}
}

showIntensityAlert(56);



